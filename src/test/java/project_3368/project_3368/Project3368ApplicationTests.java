package project_3368.project_3368;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.Repositories.CustomerRepository;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional // causes each test to clean up after itself
public class Project3368ApplicationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void testCreatingCustomer(){

        // check size before adding customer
        Iterable<Customer> customers = customerRepository.findAll();
        int sizeBefore = 0;
        int sizeAfter = 0;
        for(Customer customer1 : customers)
        {
            sizeBefore++;
        }

        Customer customer = new Customer();
        customer.setAddress("2156 Pacific");
        customer.setCity("Conroe");
        customer.setFirstName("Bob");
        customer.setLastName("Smith");
        customer.setPhoneNumber("2812654521");
        customer.setEmail("Badams@yahoo.com");
        customer.setZipcode("77381");
        customer.setState("TX");
        customerRepository.save(customer);

        // check size after adding customer
       customers = customerRepository.findAll();

       for(Customer customer1 : customers)
        {
            sizeAfter++;
        }




        // If the difference is 1, then the customer was added
        assertEquals(1,sizeAfter-sizeBefore);
    }


}
