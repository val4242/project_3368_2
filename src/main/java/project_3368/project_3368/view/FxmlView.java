package project_3368.project_3368.view;

import java.util.ResourceBundle;

/**
 * When want to switch scenes, add an Enum here and override two methods: getTitle() and getFxmlFile()
 *
 *  Access the title for each scene by using the method getStringFromResourceBundle, which obtains the
 * title from the Bundle.properties ResourceBundle
 *
 */

public enum FxmlView {
   UPDATE_JOBS{
        @Override
        public String getTitle(){return getStringFromResourceBundle("updateJobs.title");}

        @Override
        public String getFxmlFile(){return "/fxml/updateJobs.fxml";}
    },
    MANAGE_JOBS{
        @Override
        public String getTitle(){return getStringFromResourceBundle("manageJobs.title");}

        @Override
        public String getFxmlFile(){return "/fxml/manageJobs.fxml";}
    },
    ADD_CUSTOMER{
        @Override
        public String getTitle(){return getStringFromResourceBundle("addCustomer.title");}

        @Override
        public String getFxmlFile(){return "/fxml/addCustomer.fxml";}
    },
    UPDATE_CUSTOMER{
        @Override
        public String getTitle(){return getStringFromResourceBundle("updateCustomer.title");}

        @Override
        public String getFxmlFile(){return "/fxml/updateCustomer.fxml";}
    },
    SALES_REPORT{
        @Override
        public String getTitle(){return getStringFromResourceBundle("salesReport.title");}

        @Override
        public String getFxmlFile(){return "/fxml/salesReport.fxml";}
    },
    UPDATE_EMPLOYEES{
        @Override
        public String getTitle(){return getStringFromResourceBundle("updateEmployee.title");}

        @Override
        public String getFxmlFile(){return "/fxml/updateEmployee.fxml";}
    },
    ADD_EMPLOYEES{
        @Override
        public String getTitle(){return getStringFromResourceBundle("addEmployee.title");}

        @Override
        public String getFxmlFile(){return "/fxml/addEmployee.fxml";}
    },
    MANAGE_CUSTOMERS{
        @Override
        public String getTitle(){return getStringFromResourceBundle("manageCustomers.title");}

        @Override
        public String getFxmlFile(){return "/fxml/manageCustomers.fxml";}
    },
   MANAGEEMPLOYEES{
        @Override
        public String getTitle(){return getStringFromResourceBundle("manageEmployees.title");}

        @Override
        public String getFxmlFile(){return "/fxml/manageEmployees.fxml";}
    },
    NEWCUSTOMER{
        @Override
        public String getTitle(){return getStringFromResourceBundle("newCustomerInfo.title");}

        @Override
        public String getFxmlFile(){return  "/fxml/newCustomerInfo.fxml";}
    },
    NEWJOBP1{
        @Override
        public String getTitle(){return getStringFromResourceBundle("newJobPage1.title");}

        @Override
        public String getFxmlFile(){return  "/fxml/newJobPage1.fxml";}
    },
    JOB{
        @Override
        public String getTitle(){return getStringFromResourceBundle("newjob.title");}

        @Override
        public String getFxmlFile(){return  "/fxml/newJobInfo.fxml";}
    },
    KANBAN{
        @Override
        public String getTitle(){return getStringFromResourceBundle("kanban.title");}

        @Override
        public String getFxmlFile(){return  "/fxml/kanban.fxml";}
    },
    MAIN{
        @Override
        public String getTitle(){return getStringFromResourceBundle("main.title");}

        @Override
        public String getFxmlFile(){return  "/fxml/main.fxml";}


    };

   // Override these methods for each enum
    abstract String getTitle();
    abstract  String getFxmlFile();


    // This method will search the Bundle.properties resource for the key passed to it in each enum, it will return the corresponding title
    String getStringFromResourceBundle(String key){return ResourceBundle.getBundle("Bundle").getString(key);
    }
}
