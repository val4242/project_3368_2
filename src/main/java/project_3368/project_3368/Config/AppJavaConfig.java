package project_3368.project_3368.Config;

import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import project_3368.project_3368.Logging.ExceptionWriter;
import project_3368.project_3368.view.SpringFXMLLoader;
import project_3368.project_3368.view.StageManager;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ResourceBundle;

@Configuration
public class AppJavaConfig {
    @Autowired
    SpringFXMLLoader springFXMLLoader;


    @Bean
    @Scope("prototype") //Logs Exceptions
    public ExceptionWriter exceptionWriter(){return new ExceptionWriter(new StringWriter());}

    @Bean // Accesses Resource Files
    public ResourceBundle resourceBundle(){return ResourceBundle.getBundle("Bundle");}

    @Bean
    @Lazy(value = true) // Stage/StageManager is only created after Spring context Bootstrap -- Ram Alapure Youtube
    public StageManager stageManager(Stage stage) throws IOException {
        return new StageManager(springFXMLLoader, stage);
    }
}

