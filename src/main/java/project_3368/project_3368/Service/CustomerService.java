package project_3368.project_3368.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.generic.GenericService;


public interface CustomerService extends GenericService<Customer> {

}
