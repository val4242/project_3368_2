package project_3368.project_3368.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.Service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer)
    {
        return customerRepository.save(customer);
    }

    @Override
    public void delete(Customer customer)
    {
        customerRepository.delete(customer);
    }

    @Override
    public Iterable<Customer> findAll()
    {
        return customerRepository.findAll();
    }

}
