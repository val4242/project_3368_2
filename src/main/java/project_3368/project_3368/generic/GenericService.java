package project_3368.project_3368.generic;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service interface -- Source: Ram Alapure Youtube Channel
 *
 * Has standard methods which can be implemented for all types of objects
 * @param <T>
 */


public interface GenericService<T extends  Object> {

    T save(T entity);

    T update(T entity);

    void delete(T entity);

    //void delete(Long id);

    //void deleteInBatch(List<T> entities);

    //T find(Integer id);

    Iterable<T> findAll();
}
