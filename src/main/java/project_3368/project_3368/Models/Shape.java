package project_3368.project_3368.Models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Shape implements Serializable {
    private int shapeId;
    private String shape;
    private Double price;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "shape_id", nullable = false)
    public int getShapeId() {
        return shapeId;
    }

    public void setShapeId(int shapeId) {
        this.shapeId = shapeId;
    }

    @Basic
    @Column(name = "shape", nullable = true, length = 25)
    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape1 = (Shape) o;
        return shapeId == shape1.shapeId &&
                Objects.equals(shape, shape1.shape) &&
                Objects.equals(price, shape1.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shapeId, shape, price);
    }
}
