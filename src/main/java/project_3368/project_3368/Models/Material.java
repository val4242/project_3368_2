package project_3368.project_3368.Models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Material {
    private int materialId;
    private String material;
    private Double price;

    @Override
    public String toString() {
        return material;
    }


    @Id
    @Column(name = "material_id", nullable = false)
    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    @Basic
    @Column(name = "material", nullable = true, length = 25)
    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material1 = (Material) o;
        return materialId == material1.materialId &&
                Objects.equals(material, material1.material) &&
                Objects.equals(price, material1.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(materialId, material, price);
    }
}
