package project_3368.project_3368.Models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "job_2", schema = "public", catalog = "project_3368")
public class Job2 implements Serializable {
    private int jobId;
    private Double total;
    private Integer materialId;
    private Integer colorId;
    private Integer shapeId;
    private String status;
    private Integer customerId;
    private Date dateOfSale;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id", nullable = false)
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return Integer.toString(jobId);
    }

    @Basic
    @Column(name = "total", nullable = true, precision = 0)
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Basic
    @Column(name = "material_id", nullable = true)
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Basic
    @Column(name = "color_id", nullable = true)
    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    @Basic
    @Column(name = "shape_id", nullable = true)
    public Integer getShapeId() {
        return shapeId;
    }

    public void setShapeId(Integer shapeId) {
        this.shapeId = shapeId;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 25)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "customer_id", nullable = true)
    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
/*
    @Basic
    @Column(name = "DateOfSale", nullable = true)
    public Date getDateOfSale() {
        return dateOfSale;
    }
/*
    public void setDateOfSale(Date dateOfSale) {
        this.dateOfSale = dateOfSale;
    }
*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job2 job2 = (Job2) o;
        return jobId == job2.jobId &&
                Objects.equals(total, job2.total) &&
                Objects.equals(materialId, job2.materialId) &&
                Objects.equals(colorId, job2.colorId) &&
                Objects.equals(shapeId, job2.shapeId) &&
                Objects.equals(status, job2.status) &&
                Objects.equals(customerId, job2.customerId);
               // Objects.equals(dateOfSale, job2.dateOfSale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, total, materialId, colorId, shapeId, status, customerId/*, dateOfSale*/);
    }


}
