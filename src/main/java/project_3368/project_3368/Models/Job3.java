package project_3368.project_3368.Models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "job_3", schema = "public", catalog = "project_3368")
public class Job3 implements Serializable {
    private int jobId;
    private Double total;
    private Integer materialId;
    private Integer colorId;
    private Integer shapeId;
    private String status;
    private Integer customerId;
    private Date dateofsale;
    private String jobtitle;

    /** This code sets up a many to one relationship between the Job3 class and the Customer Class
     *  This means that One Customer could have many Jobs. I would have created my Application based
     *  on these relationships, but I did not have enough time to start over as I already created my
     *  database tables and application when I tried to implement this*/
    /*
    Customer customer;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    public Customer getCustomer(){
        return customer;
    }

    public void setCustomer(Customer customer){
        this.customer = customer;
    }
    */


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id", nullable = false)
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return Integer.toString(jobId) + " " + jobtitle;
    }

    @Basic
    @Column(name = "total", nullable = true, precision = 0)
    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Basic
    @Column(name = "material_id", nullable = true)
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Basic
    @Column(name = "color_id", nullable = true)
    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    @Basic
    @Column(name = "shape_id", nullable = true)
    public Integer getShapeId() {
        return shapeId;
    }

    public void setShapeId(Integer shapeId) {
        this.shapeId = shapeId;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 25)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "customer_id", nullable = true)
    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "dateofsale", nullable = true)
    public Date getDateofsale() {
        return dateofsale;
    }

    public void setDateofsale(Date dateofsale) {
        this.dateofsale = dateofsale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job3 job3 = (Job3) o;
        return jobId == job3.jobId &&
                Objects.equals(total, job3.total) &&
                Objects.equals(materialId, job3.materialId) &&
                Objects.equals(colorId, job3.colorId) &&
                Objects.equals(shapeId, job3.shapeId) &&
                Objects.equals(status, job3.status) &&
                Objects.equals(customerId, job3.customerId) &&
                Objects.equals(dateofsale, job3.dateofsale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, total, materialId, colorId, shapeId, status, customerId, dateofsale);
    }

    @Basic
    @Column(name = "jobtitle", nullable = true, length = 50)
    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }


}
