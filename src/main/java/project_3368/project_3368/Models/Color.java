package project_3368.project_3368.Models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Color {
    private int colorId;
    private String color;
    private Double price;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "color_id", nullable = false)
    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    @Basic
    @Column(name = "color", nullable = true, length = 25)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Color color1 = (Color) o;
        return colorId == color1.colorId &&
                Objects.equals(color, color1.color) &&
                Objects.equals(price, color1.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(colorId, color, price);
    }
}
