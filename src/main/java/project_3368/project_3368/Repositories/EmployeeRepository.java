package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import project_3368.project_3368.Models.Employee;


public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
}
