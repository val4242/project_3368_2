package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project_3368.project_3368.Models.Color;

import java.util.List;

@Repository
public interface ColorRepository extends CrudRepository <Color, Integer> {
    List<Color> findByColor(String color);
}
