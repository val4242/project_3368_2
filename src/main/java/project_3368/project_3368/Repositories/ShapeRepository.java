package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project_3368.project_3368.Models.Shape;

import java.util.List;

@Repository
public interface ShapeRepository extends CrudRepository<Shape,Integer> {
    List<Shape> findByShape (String shape);
}
