package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import project_3368.project_3368.Models.Customer;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer,Integer> {
    Customer findByEmail(String email);
}
