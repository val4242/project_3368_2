package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project_3368.project_3368.Models.Material;

import java.util.List;

@Repository
public interface MaterialRepository extends CrudRepository<Material,Integer> {
    List<Material> findByMaterial(String material);
}
