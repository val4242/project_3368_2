package project_3368.project_3368.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project_3368.project_3368.Models.Job2;
import project_3368.project_3368.Models.Job3;

import java.sql.Date;
import java.util.List;

@Repository
public interface Job3Repository extends CrudRepository<Job3,Integer> {
    Iterable<Job3> findByStatus (String status);
}
