package project_3368.project_3368;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

/**
 * Launches Application -- Credit: Ram Alapure Youtube
 */


@SpringBootApplication
public class Project3368Application extends Application {

    protected ConfigurableApplicationContext springContext;
    protected StageManager stageManager;

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        springContext = springBootApplicationContext();

    }

    @Override
    public void start(Stage stage) throws Exception {
        stageManager = springContext.getBean(StageManager.class, stage);
        displayInitialScene();


    }
    @Override
    public void stop() throws Exception{
        springContext.close();
    }


    protected void displayInitialScene(){stageManager.switchScene(FxmlView.MAIN);}


    private ConfigurableApplicationContext springBootApplicationContext() {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Project3368Application.class);
        String[] args = getParameters().getRaw().stream().toArray(String[]::new);
        builder.headless(false);
        return builder.run(args);
    }
}
