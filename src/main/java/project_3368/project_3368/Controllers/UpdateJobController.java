package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.ColorRepository;
import project_3368.project_3368.Repositories.Job3Repository;
import project_3368.project_3368.Repositories.MaterialRepository;
import project_3368.project_3368.Models.Color;
import project_3368.project_3368.Models.Material;
import project_3368.project_3368.Models.Shape;
import project_3368.project_3368.Repositories.ShapeRepository;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import static project_3368.project_3368.Controllers.ManageJobsController.job3;

@Controller
public class UpdateJobController implements Initializable {

    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    Job3Repository job3Repository;

    @Autowired
    MaterialRepository materialRepository;

    @Autowired
    ColorRepository colorRepository;

    @Autowired
    ShapeRepository shapeRepository;

    @FXML
    private AnchorPane content;

    @FXML
    private Button updateButton;

    @FXML
    private Button backButton;

    @FXML
    private Button homeButton;

    @FXML
    private ComboBox materialComboBox;

    @FXML
    private ComboBox colorComboBox;

    @FXML
    private ComboBox shapeComboBox;

    @FXML
    private Label materialPriceLabel;

    @FXML
    private Label colorPriceLabel;

    @FXML
    private Label shapePriceLabel;

    @FXML
    private Label totalLabel;

    @FXML
    private Label DateOfSaleLabel;

    @FXML
    private Label stateTaxLabel;

    @FXML
    private Label markupLabel;

    @FXML
    void backButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.MANAGE_JOBS);
    }

    @FXML
    void colorSelected(ActionEvent event) {
        String colorPrice = " ";

        List<Color> colors = colorRepository.findByColor(colorComboBox.getSelectionModel().getSelectedItem().toString());

        for (Color color : colors) {
            colorPrice = color.getPrice().toString();
        }


        colorPriceLabel.setText(colorPrice);

        calcTotal();
    }

    @FXML
    void homeButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);
    }

    @FXML
    void materialSelected(ActionEvent event) {

        String materialPrice = " ";

        List<Material> materials = materialRepository.findByMaterial(materialComboBox.getSelectionModel().getSelectedItem().toString());

        for (Material material : materials) {
            materialPrice = material.getPrice().toString();
        }


        materialPriceLabel.setText(materialPrice);

        calcTotal();
    }

    @FXML
    void shapeSelected(ActionEvent event) {
        String shapePrice = " ";

        List<Shape> shapes = shapeRepository.findByShape(shapeComboBox.getSelectionModel().getSelectedItem().toString());

        for (Shape shape : shapes) {
            shapePrice = shape.getPrice().toString();
        }


        shapePriceLabel.setText(shapePrice);

        calcTotal();

    }

    @FXML
    void updateButtonPushed(ActionEvent event) {
        Iterable<Material> materials;
        materials = materialRepository.findByMaterial(materialComboBox.getValue().toString());
        materials.forEach(material -> job3.setMaterialId(material.getMaterialId()));


        Iterable<Color> colors;
        colors = colorRepository.findByColor(colorComboBox.getValue().toString());
        colors.forEach(color -> job3.setColorId(color.getColorId()));

        Iterable<Shape> shapes;
        shapes = shapeRepository.findByShape(shapeComboBox.getValue().toString());
        shapes.forEach(shape -> job3.setShapeId(shape.getShapeId()));

        job3.setJobtitle(materialComboBox.getValue().toString() + " " + colorComboBox.getValue().toString() + " " +
                         shapeComboBox.getValue().toString());

        job3.setTotal(Double.parseDouble(totalLabel.getText()));

        job3Repository.save(job3);

        stageManager.switchScene(FxmlView.MANAGE_JOBS);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Optional<Material> materials = materialRepository.findById(job3.getMaterialId());

        materialComboBox.getItems().removeAll(materialComboBox.getItems());
        materialComboBox.getItems().addAll("Burlap", "Cotton", "Velvet", "Silk");
        materialComboBox.getSelectionModel().select(materials.get().getMaterial());
        materialPriceLabel.setText(Double.toString(materials.get().getPrice()));

        Optional<Color> colors = colorRepository.findById(job3.getColorId());

        colorComboBox.getItems().removeAll(colorComboBox.getItems());
        colorComboBox.getItems().addAll("White", "Red", "Green", "Gold");
        colorComboBox.getSelectionModel().select(colors.get().getColor());
        colorPriceLabel.setText(Double.toString(colors.get().getPrice()));


        Optional<Shape> shapes = shapeRepository.findById(job3.getShapeId());

        shapeComboBox.getItems().removeAll(shapeComboBox.getItems());
        shapeComboBox.getItems().addAll("Sock", "Bone", "Candy Cane", "Paw Print");
        shapeComboBox.getSelectionModel().select(shapes.get().getShape());

        shapePriceLabel.setText(Double.toString(shapes.get().getPrice()));
        calcTotal();
    }

    public void calcTotal(){
        double num1;
        double num2;
        double num3;
        double subtotal;
        double total;
        double markup;
        double stateTax;

        num1 = Double.parseDouble(materialPriceLabel.getText());
        num2 = Double.parseDouble(colorPriceLabel.getText());
        num3 = Double.parseDouble(shapePriceLabel.getText());
        subtotal = num1 + num2 + num3;
        markup = (.10*subtotal);
        subtotal = subtotal +markup;
        markupLabel.setText(Double.toString(markup));
        stateTax = .034*subtotal;
        stateTaxLabel.setText(Double.toString(stateTax));
        total = subtotal+stateTax;

        totalLabel.setText(Double.toString(total));
    }

}