package project_3368.project_3368.Controllers;

import project_3368.project_3368.Service.CustomerService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.io.IOException;


@Controller
public class AddCustomerController {

    @FXML
    private TextField firstNameTxtBox;
    @FXML
    private TextField lastNameTxtBox;
    @FXML
    private TextField phoneTxtBox;
    @FXML
    private TextField emailTxtBox;
    @FXML
    private TextField addressTxtBox;
    @FXML
    private TextField cityTxtBox;
    @FXML
    private TextField stateTxtBox;
    @FXML
    private TextField zipTxtBox;
    @FXML
    private Button backButton;
    @FXML
    private Button homeButton;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @Lazy
    @Autowired
    StageManager stageManager;

    /** Once User inputs all Customer information and presses that Add Button, the application creates a new Customer
     * and retrieves and sets all of the information based on the text in the respective textboxes. Finally, the customer
     * is added to the database, and the stageManager is called to switch stages back to the Manage_Customers Window.
     * @param actionEvent
     */
    public void addButtonPushed(ActionEvent actionEvent) {

        int num = checkInput();

        // if all fields are filled in then add customer
        if(num == 0) {
            Customer customer = new Customer();

            customer.setFirstName(firstNameTxtBox.getText());
            customer.setLastName(lastNameTxtBox.getText());
            customer.setPhoneNumber(phoneTxtBox.getText());
            customer.setEmail(emailTxtBox.getText());
            customer.setAddress(addressTxtBox.getText());
            customer.setCity(cityTxtBox.getText());
            customer.setState(stateTxtBox.getText());
            customer.setZipcode(zipTxtBox.getText());

            /**
             * Uses the CustomerService Service to save the customer!
             */
            customerService.save(customer);
            //customerRepository.save(customer);

            stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS);
        }

    }

    /**
     * Validation -- check if all fields are filled in
     * @return
     */
    public int checkInput() {
        if (firstNameTxtBox.getText().isEmpty() || lastNameTxtBox.getText().isEmpty() || phoneTxtBox.getText().isEmpty()
                || emailTxtBox.getText().isEmpty() || addressTxtBox.getText().isEmpty() || cityTxtBox.getText().isEmpty() ||
                stateTxtBox.getText().isEmpty() || zipTxtBox.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please input all information!");
            alert.showAndWait();
            return 1;
        }
        return 0;
    }

    /** Switch Stages**/
    public void backButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS);
    }

    public void homeButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.KANBAN);
    }

}
