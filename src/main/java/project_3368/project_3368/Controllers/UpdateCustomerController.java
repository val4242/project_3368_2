package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;
import java.net.URL;
import java.util.ResourceBundle;

import static project_3368.project_3368.Controllers.ManageCustomersController.customer;

@Controller
public class UpdateCustomerController implements Initializable {

    @Autowired
    CustomerRepository customerRepository;

    @Lazy
    @Autowired
    StageManager stageManager;

    @FXML
    private Button BackButton;

    @FXML
    private Button HomeButton;

    @FXML
    private Button UpdateButton;

    @FXML
    private TextField firstNameTxtBox;

    @FXML
    private TextField lastNameTxtBox;

    @FXML
    private TextField phoneTxtBox;

    @FXML
    private TextField emailTxtBox;

    @FXML
    private TextField addressTxtBox;

    @FXML
    private TextField cityTxtBox;

    @FXML
    private TextField stateTxtBox;

    @FXML
    private TextField zipTxtBox;


    @FXML
    void UpdateButtonPushed(ActionEvent event) {
        customer.setFirstName(firstNameTxtBox.getText());
        customer.setAddress(addressTxtBox.getText());
        customer.setLastName(lastNameTxtBox.getText());
        customer.setPhoneNumber(phoneTxtBox.getText());
        customer.setEmail(emailTxtBox.getText());
        customer.setAddress(addressTxtBox.getText());
        customer.setCity(cityTxtBox.getText());
        customer.setState(stateTxtBox.getText());
        customer.setZipcode(zipTxtBox.getText());

        customerRepository.save(customer);

        stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       addressTxtBox.setText(customer.getAddress());
        firstNameTxtBox.setText(customer.getFirstName());
        lastNameTxtBox.setText(customer.getLastName());
        phoneTxtBox.setText(customer.getPhoneNumber());
        addressTxtBox.setText(customer.getAddress());
        stateTxtBox.setText(customer.getState());
        zipTxtBox.setText(customer.getZipcode());
        cityTxtBox.setText(customer.getCity());
        emailTxtBox.setText(customer.getEmail());
    }

    /**
     * Change Scenes
     * @param event
     */
    @FXML
    void BackButtonPushed(ActionEvent event) { stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS); }

    @FXML
    void HomeButtonPushed(ActionEvent event) { stageManager.switchScene(FxmlView.KANBAN);}

}
