package project_3368.project_3368.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.EmployeeRepository;
import project_3368.project_3368.Models.Employee;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class ManageEmployeeController implements Initializable {

    /**
     * This Controller allows the Shop owner to create, update, delete and view employees
     */

    static Employee employee;
    public Button UpdateEmployeeButton;

    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    EmployeeRepository employeeRepository;

    @FXML
    private TableView <Employee> EmployeeTable;
    @FXML
    private TableColumn <Employee,String> EmployeeNumberColumn;
    @FXML
    private TableColumn <Employee,String> FirstNameColumn;
    @FXML
    private TableColumn <Employee,String> LastNameColumn;
    @FXML
    private TableColumn <Employee,String>PositionColumn;
    @FXML
    private TableColumn <Employee,String>PhoneNumberColumn;
    @FXML
    private Button AddEmployeeButton;
    @FXML
    private Button HomeButton;
    @FXML
    private Button DeleteEmployeeButton;

    /**
     * Saves the selected employee to the static variable employee and switches stages to the update employee stage.
     * @param actionEvent
     */
    public void UpdateEmployeeButtonPushed(ActionEvent actionEvent) {
       employee = EmployeeTable.getSelectionModel().getSelectedItem();
        stageManager.switchScene(FxmlView.UPDATE_EMPLOYEES);
    }

    /**
     * When User selects a employee from the tableview and presses the delete button, this method is executed and
     * deletes the corresponding selected employee from the database. The Tableview is reloaded with the updated data.
     * @param actionEvent
     */
    public void DeleteEmployeeButtonPushed(ActionEvent actionEvent) {
        employee = EmployeeTable.getSelectionModel().getSelectedItem();
        employeeRepository.delete(employee);
        initializeTV();
    }


    /**
     * Runs when the stage is first loaded
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTV();

    }

    /**
     * InitializeTV is called in the Initialize method, this populates the tableview
     */

    public void initializeTV(){

        ObservableList<Employee> obsEmployeeList = FXCollections.observableArrayList();

        Iterable<Employee> employeeList = employeeRepository.findAll();

        employeeList.forEach(employee->obsEmployeeList.add(employee));



        EmployeeNumberColumn.setCellValueFactory(new PropertyValueFactory<>("employeeNumber"));
        FirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        LastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        PositionColumn.setCellValueFactory(new PropertyValueFactory<>("position"));
        PhoneNumberColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));

        EmployeeTable.setItems(obsEmployeeList);
        EmployeeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Switch Scenes
     * @param actionEvent
     */
    public void HomeButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.KANBAN);
    }

    public void AddEmployeeButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.ADD_EMPLOYEES);
    }
}
