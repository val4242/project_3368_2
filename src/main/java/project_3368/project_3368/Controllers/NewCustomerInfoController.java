package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.io.IOException;



@Controller
public class NewCustomerInfoController {

    /**
     * Creates a new Customer when filling out a new job order form
     */

    static Customer customer2 = new Customer();


    @FXML
    private TextField firstNameTxtBox;
    @FXML
    private TextField lastNameTxtBox;
    @FXML
    private TextField phoneTxtBox;
    @FXML
    private TextField emailTxtBox;
    @FXML
    private TextField addressTxtBox;
    @FXML
    private TextField cityTxtBox;
    @FXML
    private TextField stateTxtBox;
    @FXML
    private TextField zipTxtBox;
    @FXML
    private Button nextButton;
    @FXML
    private Button backButton;
    @FXML
    private Button homeButton;

    @Autowired
    CustomerRepository customerRepository;

    @Lazy
    @Autowired
    StageManager stageManager;

    public void nextButtonPushed(ActionEvent actionEvent) throws IOException {

        int check = checkInput();

        // if all fields are filled in
        if (check == 0) {

            Customer customer = new Customer();
            customer.setFirstName(firstNameTxtBox.getText());
            customer.setLastName(lastNameTxtBox.getText());
            customer.setPhoneNumber(phoneTxtBox.getText());
            customer.setEmail(emailTxtBox.getText());
            customer.setAddress(addressTxtBox.getText());
            customer.setCity(cityTxtBox.getText());
            customer.setState(stateTxtBox.getText());
            customer.setZipcode(zipTxtBox.getText());

            customer2 = customer;

            stageManager.switchScene(FxmlView.JOB);
        }
    }

    /**
     * Validation -- check if all fields are filled in
     * @return
     */
    public int checkInput() {
        if (firstNameTxtBox.getText().isEmpty() || lastNameTxtBox.getText().isEmpty() || phoneTxtBox.getText().isEmpty()
                || emailTxtBox.getText().isEmpty() || addressTxtBox.getText().isEmpty() || cityTxtBox.getText().isEmpty() ||
                stateTxtBox.getText().isEmpty() || zipTxtBox.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please input all information!");
            alert.showAndWait();
            return 1;
        }
        return 0;
    }

    /**
     * Switch Scenes
     * @param actionEvent
     * @throws IOException
     */
    public void backButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.KANBAN);
    }

    public void homeButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.KANBAN);
    }
}
