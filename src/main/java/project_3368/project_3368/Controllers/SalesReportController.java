package project_3368.project_3368.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.Job3Repository;
import project_3368.project_3368.Models.Job3;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;
import java.net.URL;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

@Controller
public class SalesReportController implements Initializable {

    public Button generateButton;
    public DatePicker startDate;
    public DatePicker endDate;

    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    Job3Repository job3Repository;

    public Label totalSalesLabel;

    @FXML
    private TableView<Job3> SalesTable;

    @FXML
    private TableColumn<Job3, Integer> jobIDcol;

    @FXML
    private TableColumn<Job3, Integer> custIDcol;

    @FXML
    private TableColumn<Job3, String> statusCol;

    @FXML
    private TableColumn<Job3, Double> SaleCol;

    @FXML
    private TableColumn<Job3, Date> DateCol;

    @FXML
    private TableColumn<Job3,String> jobTitle;

    @FXML
    private Button HomeButton;

    @FXML
    private TextField startMonthTxtBox;

    @FXML
    private TextField startDayTxtBox;

    @FXML
    private TextField startDayTxtBox1;

    @FXML
    private TextField endMonthTxtBox;

    @FXML
    private TextField endDayTxtBox;

    @FXML
    private TextField endYearTxtBox;

    // Used to format Doubles to 2 decimal places
    DecimalFormat df2 = new DecimalFormat(".##");


    public void generateButtonPushed(ActionEvent actionEvent){

        // Keeps track of total
        Double newTotal = 0.0;

        ObservableList<Job3> obsJobList = FXCollections.observableArrayList();

        // Clear Sales Table each time for new set of data
        SalesTable.getItems().clear();

        // Get Date Values from Date Pickers
        Date startDateSQL = Date.valueOf(startDate.getValue());
        Date endDateSQL = Date.valueOf(endDate.getValue());

        // Get all Jobs from Database
        Iterable<Job3> job3List = job3Repository.findAll();

        /* Iterate through all jobs, if they are within the user-defined date range, then they will be added to
            an observable list for display and the corresponding totals will be added to keep track of overall sales
         */
        for (Job3 job3 : job3List) {
            if (((job3.getDateofsale().compareTo(startDateSQL) > 0) && (job3.getDateofsale().compareTo(endDateSQL) < 0))
                    || (job3.getDateofsale().compareTo(startDateSQL) == 0) || (job3.getDateofsale().compareTo(endDateSQL) == 0)) {

                // Add job to observable list for TableView
                obsJobList.add(job3);

                // Calculate total Sales
                Double total = job3.getTotal();
                newTotal += total;
            }
        }

        // Display Total Sales
        totalSalesLabel.setText("$ " + df2.format(newTotal));

        // Display Jobs within date range
        jobIDcol.setCellValueFactory(new PropertyValueFactory<>("jobId"));
        custIDcol.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        SaleCol.setCellValueFactory(new PropertyValueFactory<>("total"));
        DateCol.setCellValueFactory(new PropertyValueFactory<>("dateofsale"));
        jobTitle.setCellValueFactory(new PropertyValueFactory<>("jobtitle"));

        SalesTable.setItems(obsJobList);
        SalesTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

}

    /**
     * Switch Stages
     * @param event
     */

    @FXML
    void HomeButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);

    }

    /**
     * Methods with no action yet
     * @param actionEvent
     */

    public void startDateSelected(ActionEvent actionEvent) {

    }

    public void endDateSelected(ActionEvent actionEvent) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
