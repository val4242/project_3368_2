package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.EmployeeRepository;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.net.URL;
import java.util.ResourceBundle;

import static project_3368.project_3368.Controllers.ManageEmployeeController.employee;

@Controller
public class UpdateEmployeeController implements Initializable {

    @Autowired
    EmployeeRepository employeeRepository;

    @Lazy
    @Autowired
    StageManager stageManager;

    @FXML
    private TextField FNameTxtBox;

    @FXML
    private TextField PositionTxtBox;

    @FXML
    private TextField StatusTxtBox;

    @FXML
    private TextField PhoneTxtBox;

    @FXML
    private TextField EmailTxtBox;

    @FXML
    private TextField LNameTxtBox;

    @FXML
    private TextField AddressTxtBox;

    @FXML
    private TextField CityTxtBox;

    @FXML
    private TextField StateTxtBox;

    @FXML
    private TextField ZipTxtBox;

    @FXML
    private TextField EmpNumTxtBox;

    @FXML
    private Button BackButton;

    @FXML
    private Button HomeButton;

    @FXML
    private Button UpdateButton;


    @FXML
    void UpdateButtonPushed(ActionEvent event) {
        employee.setFirstName(FNameTxtBox.getText());
        employee.setAddress(AddressTxtBox.getText());
        employee.setLastName(LNameTxtBox.getText());
        employee.setPosition(PositionTxtBox.getText());
        employee.setStatus(StatusTxtBox.getText());
        employee.setPhoneNumber(PhoneTxtBox.getText());
        employee.setEmail(EmailTxtBox.getText());
        employee.setAddress(AddressTxtBox.getText());
        employee.setCity(CityTxtBox.getText());
        employee.setState(StateTxtBox.getText());
        employee.setZipcode(ZipTxtBox.getText());
        employee.setEmployeeNumber((EmpNumTxtBox.getText()));

        employeeRepository.save(employee);

        stageManager.switchScene(FxmlView.MANAGEEMPLOYEES);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
            AddressTxtBox.setText(employee.getAddress());
            FNameTxtBox.setText(employee.getFirstName());
            LNameTxtBox.setText(employee.getLastName());
            PositionTxtBox.setText(employee.getPosition());
            StatusTxtBox.setText(employee.getStatus());
            PhoneTxtBox.setText(employee.getPhoneNumber());
            EmailTxtBox.setText(employee.getEmail());
            AddressTxtBox.setText(employee.getAddress());
            CityTxtBox.setText(employee.getCity());
            StateTxtBox.setText(employee.getState());
            ZipTxtBox.setText(employee.getZipcode());
            EmpNumTxtBox.setText(employee.getEmployeeNumber());
    }

    /**
     * Change Scenes
     */
    @FXML
    void BackButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS);
    }

    @FXML
    void HomeButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);
    }

}
