package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.io.IOException;


@Controller
public class NewJobPage1Controller {

    @Lazy
    @Autowired
    StageManager stageManager;

    public void newCustomerButtonPushed(ActionEvent actionEvent) throws IOException {

        stageManager.switchScene(FxmlView.JOB);
    }

    public void existingCustomerButtonPushed(ActionEvent actionEvent) throws IOException {

    }

    public void homeButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.KANBAN);
    }
}
