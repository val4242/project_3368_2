package project_3368.project_3368.Controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.fxml.Initializable;
import java.io.IOException;
import java.util.List;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.Job3Repository;
import project_3368.project_3368.Models.Job3;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;


@Controller
public class KanbanController implements Initializable{



    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    Job3Repository job3Repository;

    public ListView<Job3> PreproductionList;
    public ListView<Job3> ProductionList;
    public ListView<Job3> PostProductionList;
    public Button newJobButton;
    public Button archiveButton;

    private static final DataFormat JOB_LIST = new DataFormat("project_3368/personList");

    /**
     * Detects when user clicks and drags item from the preproduciton column
     * @param mouseEvent
     */

    public void dragSourceDetected(MouseEvent mouseEvent) {

        // get the number of items that were selected
        int selected = PreproductionList.getSelectionModel().getSelectedIndices().size();

        // If at least one item was selected then start drag
        if(selected > 0){
            Dragboard dragboard = PreproductionList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            //create an ArrayList of Jobs that were selected
            ArrayList<Job3> selectedItems = new ArrayList<>(PreproductionList.getSelectionModel().getSelectedItems());
            // Create new Clipboard content object
            ClipboardContent content = new ClipboardContent();
            //add the selected items using the dataformat key JOB_LIST to the clipboard
            content.put(JOB_LIST,selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }


    }

    /**
     * Detects when user has dragged an item from the preproduction column over the production column
     * @param dragEvent
     */
    public void dragOverTarget(DragEvent dragEvent) {

        Dragboard dragboard = dragEvent.getDragboard();
        // if the dragboard contains content from the JOB_LIST key, then the production column accepts a transfermode of Move
        if(dragboard.hasContent(JOB_LIST)){
            dragEvent.acceptTransferModes(TransferMode.MOVE);
        }
        dragEvent.consume();


    }

    /**
     * Detects when user drops a dragged item into the production column, updates status of job to Production
     * @param dragEvent
     */

    public void dragDroppedOnTarget(DragEvent dragEvent) {

        int confirm = confirmStatusChange();

        if(confirm == 0) {
            boolean dragCompleted = false;
            Dragboard dragboard = dragEvent.getDragboard();
            if (dragboard.hasContent(JOB_LIST)) {


                //Display all items in the Production list, including the newly dropped item
                ArrayList<Job3> jobs = (ArrayList<Job3>) dragboard.getContent(JOB_LIST); // cast object into ArrayList
                ProductionList.getItems().addAll(jobs);
                dragCompleted = true;

            }
            dragEvent.setDropCompleted(dragCompleted);
            dragEvent.consume();
        }

    }


    /**
     * Once the Drag is completely over, the items are removed from the preproduction list
     * @param dragEvent
     */
    public void DragDoneOnSource(DragEvent dragEvent) {

            TransferMode tm = dragEvent.getAcceptedTransferMode();
            if(tm == TransferMode.MOVE) {
                removeSelectedItems();
            }
            dragEvent.consume();

        }

    /**
     * Detects user clicking and dragging an item in the production list
     * @param mouseEvent
     */
    public void dragProductionDetected(MouseEvent mouseEvent) {
        int selected = ProductionList.getSelectionModel().getSelectedIndices().size();

        if(selected > 0){
            Dragboard dragboard = ProductionList.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<Job3> selectedItems = new ArrayList<>(ProductionList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(JOB_LIST,selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }
    }

    /**
     * Detects when user has dragged an item from the production list over the post-production/closeout
     * @param dragEvent
     */
    public void dragOverPostProduction(DragEvent dragEvent) {

        System.out.println("Drag over detected");
        Dragboard dragboard = dragEvent.getDragboard();
        if(dragboard.hasContent(JOB_LIST)){
            dragEvent.acceptTransferModes(TransferMode.MOVE);

        }
        dragEvent.consume();

    }

    /**
     * Detects user dropping an item into the post-production list and updates the status of the job to Post-Production
     * @param dragEvent
     */
    public void dragDroppedOnPostProduction(DragEvent dragEvent) {

        int confirm = confirmStatusChange();

        if(confirm == 0) {
            boolean dragCompleted = false;
            Dragboard dragboard = dragEvent.getDragboard();
            if (dragboard.hasContent(JOB_LIST)) {

                // display all items in the Post Production list including the newly added jobs
                ArrayList<Job3> jobs = (ArrayList<Job3>) dragboard.getContent(JOB_LIST);
                PostProductionList.getItems().addAll(jobs);

                dragCompleted = true;

            }
            dragEvent.setDropCompleted(dragCompleted);
            dragEvent.consume();
        }
    }

    /**
     * Detects when the drag event is done from the Production list and removes the corresponding item(s)
     * from the production list, and updates the status of the job
     * @param dragEvent
     */
    public void DragDoneOnProduction(DragEvent dragEvent) {
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if(tm == TransferMode.MOVE) {
            removeSelectedItemsProduction();
        }
        dragEvent.consume();
    }


    /**
     * Change the Status of a Job to Archived
     * @param actionEvent
     */
    public void archiveButtonPushed(ActionEvent actionEvent) {

        List<Job3> selectedJobs = new ArrayList<>();
        // add selected jobs to selectedJobs list
        for(Job3 job3 : PostProductionList.getSelectionModel().getSelectedItems()){
            selectedJobs.add(job3);
        }
        PostProductionList.getSelectionModel().clearSelection();

        // Change job status to archived and saves the job with the updated status
        for (Job3 job3 : selectedJobs) {
            job3.setStatus("Archived");
           job3Repository.save(job3);

        }
        // remove all jobs that were selected when user clicked the archive button
       PostProductionList.getItems().removeAll(selectedJobs);
    }


    /**
     * ********** Credit: Professor Lancaster in his Drag and Drop Sample************
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // initialize observable list for display
        ObservableList<Job3> preproductionObsList = FXCollections.observableArrayList();
        ObservableList<Job3> productionObsList = FXCollections.observableArrayList();
        ObservableList<Job3> postproductionObsList = FXCollections.observableArrayList();

        // Retrieve people by status
        Iterable<Job3> preproductionListJobs = job3Repository.findByStatus("Pre-Production");
        Iterable<Job3> productionListJobs = job3Repository.findByStatus("Production");
        Iterable<Job3> postProductionListJobs = job3Repository.findByStatus("Post-Production");

        //Essentially convert Iterable list into Observable list for display
        preproductionListJobs.forEach(job3 -> preproductionObsList.add(job3));
        productionListJobs.forEach(job3 -> productionObsList.add(job3));
        postProductionListJobs.forEach(job3->postproductionObsList.add(job3));

        //set jobs in preproduciton list view
        PreproductionList.setItems(preproductionObsList);
        PreproductionList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //set jobs in production list view
        ProductionList.setItems(productionObsList);
        ProductionList.getSelectionModel().setSelectionMode((SelectionMode.MULTIPLE));

        //set jobs in Post-Production list view
        PostProductionList.setItems(postproductionObsList);
        PostProductionList.getSelectionModel().setSelectionMode((SelectionMode.MULTIPLE));
    }


    /**
     * Methods for removing items after the drag and drop is done and changes their status
     */
    // removes selected items from the preproduction list, set status to production
    private void removeSelectedItems() {
        List<Job3> selectedFolk = new ArrayList<>();
        for(Job3 job3 : PreproductionList.getSelectionModel().getSelectedItems()){
            selectedFolk.add(job3);
        }
        PreproductionList.getSelectionModel().clearSelection();

        for (Job3 job3 : selectedFolk) {
            job3.setStatus("Production");
            job3Repository.save(job3);

        }

        PreproductionList.getItems().removeAll(selectedFolk);
    }

    // removes selected items from the production list, set status to Post-Production
    private void removeSelectedItemsProduction() {
        List<Job3> selectedFolk = new ArrayList<>();

        // For each item selected, add the item to the selected Folk ArrayList
        for(Job3 job3 :ProductionList.getSelectionModel().getSelectedItems()){
            selectedFolk.add(job3);
        }
        ProductionList.getSelectionModel().clearSelection();

        // For each job in the selectedFolk ArrayList, set the status to Post-Production and save the job
        for (Job3 job3 : selectedFolk) {
            job3.setStatus("Post-Production");
            job3Repository.save(job3);

        }

        // Remove selected items from Production
        ProductionList.getItems().removeAll(selectedFolk);
    }

    public int confirmStatusChange()
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you would like to Change the status?"
                , ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if(alert.getResult() == ButtonType.YES)
        {
            return 0;
        }

        return 1;
    }

    /**
     * Switch Stages
     * @param actionEvent
     */

    public void QuoteButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.JOB);
    }

    public void salesReportButtonPushed(ActionEvent actionEvent) {stageManager.switchScene(FxmlView.SALES_REPORT);}

    public void employeeButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.MANAGEEMPLOYEES);
    }

    public void back(ActionEvent actionEvent) { stageManager.switchScene(FxmlView.MAIN); }

    public void newJobButtonPressed(ActionEvent actionEvent) throws IOException { stageManager.switchScene(FxmlView.NEWCUSTOMER); }

    public void customerButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.MANAGE_CUSTOMERS);
    }

    public void logoutButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.MAIN);
    }

    public void jobButtonPushed(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.MANAGE_JOBS);
    }

}
