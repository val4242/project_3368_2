package project_3368.project_3368.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.Job3Repository;
import project_3368.project_3368.Models.Job3;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

@Controller
public class ManageJobsController implements Initializable {

    static Job3 job3 = new Job3();

    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    Job3Repository job3Repository;


    public Label totalSalesLabel;

    @FXML
    private TableView<Job3> SalesTable;

    @FXML
    private TableColumn<Job3, Integer> jobIDcol;

    @FXML
    private TableColumn<Job3, Integer> custIDcol;

    @FXML
    private TableColumn<Job3, String> statusCol;

    @FXML
    private TableColumn<Job3, Double> SaleCol;

    @FXML
    private TableColumn<Job3, Date> DateCol;

    @FXML
    private TableColumn<Job3,String> jobTitle;

    @FXML
    private Button HomeButton;

    @FXML
    private Button addJobButton;

    @FXML
    private Button updateJobButton;

    @FXML
    private Button deleteJobButton;


    @FXML
    void deleteJobButton(ActionEvent event) {
        job3 = SalesTable.getSelectionModel().getSelectedItem();
        job3Repository.delete(job3);
        initializeTV();
    }

    @FXML
    void updateJobButton(ActionEvent event) {
       job3 = SalesTable.getSelectionModel().getSelectedItem();
        stageManager.switchScene(FxmlView.UPDATE_JOBS);
    }


    /**
     * Runs when Stage is first Loaded
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTV();
    }

    /**
     * InitializeTV is called in the Initialize method, this populates the tableview
     */
    public void initializeTV(){
        ObservableList<Job3> obsJobList = FXCollections.observableArrayList();

        Iterable<Job3> jobs = job3Repository.findAll();

        jobs.forEach(job3->obsJobList.add(job3));


        jobIDcol.setCellValueFactory(new PropertyValueFactory<>("jobId"));
        custIDcol.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        SaleCol.setCellValueFactory(new PropertyValueFactory<>("total"));
        DateCol.setCellValueFactory(new PropertyValueFactory<>("dateofsale"));
        jobTitle.setCellValueFactory(new PropertyValueFactory<>("jobtitle"));

        SalesTable.setItems(obsJobList);
        SalesTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Switch Stages
     * @param event
     */
    @FXML
    void HomeButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);
    }

    @FXML
    void addJobButton(ActionEvent event) {
        stageManager.switchScene(FxmlView.NEWCUSTOMER);
    }

}
