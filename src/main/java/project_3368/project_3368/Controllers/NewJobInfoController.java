package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Models.*;
import project_3368.project_3368.Repositories.*;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import static project_3368.project_3368.Controllers.NewCustomerInfoController.customer2;


@Controller
public class NewJobInfoController implements Initializable {


    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    CustomerRepository customerRepository;


    @Autowired
    MaterialRepository materialRepository;

    @Autowired
    ColorRepository colorRepository;

    @Autowired
    ShapeRepository shapeRepository;

    @Autowired
    Job3Repository job3Repository;

    @FXML
    private Button homeButton;
    @FXML
    private AnchorPane content;
    @FXML
    private ComboBox materialComboBox;
    @FXML
    private ComboBox colorComboBox;
    @FXML
    private ComboBox shapeComboBox;
    @FXML
    private Label materialPriceLabel;
    @FXML
    private Label colorPriceLabel;
    @FXML
    private Label shapePriceLabel;
    @FXML
    private Label totalLabel;
    @FXML
    private Label DateOfSaleLabel;
    @FXML
    private Label stateTaxLabel;
    @FXML
    private Label markupLabel;
    @FXML
    private TextField jobTitleTxtBox;


    public void materialSelected(ActionEvent actionEvent) {

        String materialPrice = " ";

        //Retrieve value stored in combo box to search the corresponding material in the material table and save it in a list
        List<Material> materials = materialRepository.findByMaterial(materialComboBox.getSelectionModel().getSelectedItem().toString());

        //For each material in the list, get the corresponding price as a String
        for (Material material : materials) {
            materialPrice = material.getPrice().toString();
        }

        materialPriceLabel.setText(materialPrice);

        calculateJobTotal();

    }

    public void colorSelected(ActionEvent actionEvent) {

        String colorPrice = " ";

        List<Color> colors = colorRepository.findByColor(colorComboBox.getSelectionModel().getSelectedItem().toString());

        for (Color color : colors) {
            colorPrice = color.getPrice().toString();
        }

        calculateJobTotal();

        colorPriceLabel.setText(colorPrice);


    }


    public void shapeSelected(ActionEvent actionEvent) {
        String shapePrice = " ";

        List<Shape> shapes = shapeRepository.findByShape(shapeComboBox.getSelectionModel().getSelectedItem().toString());

        for (Shape shape : shapes) {
            shapePrice = shape.getPrice().toString();
        }

        shapePriceLabel.setText(shapePrice);
        calculateJobTotal();
    }

    public void submitButtonPushed(ActionEvent actionEvent) throws IOException {

        // Save the customer with the info entered on previous page (newCustomerInfo page)
        customerRepository.save(customer2);

        int colorID = 0;
        int materialID = 0;
        int shapeID = 0;

        // Create new Job3 and set the fields
        Job3 job = new Job3();
        job.setStatus("Pre-Production");
        job.setTotal(Double.parseDouble(totalLabel.getText()));

        // get color value from colorComboBox
        List<Color> colors = colorRepository.findByColor(colorComboBox.getSelectionModel().getSelectedItem().toString());

        // Retrieve corresponding colorID
        for (Color color : colors) {
            colorID = color.getColorId();
        }

        // set job ColorID
        job.setColorId(colorID);

        List<Material> materials = materialRepository.findByMaterial(materialComboBox.getSelectionModel().getSelectedItem().toString());

        for (Material material : materials) {
            materialID = material.getMaterialId();
        }

        job.setMaterialId(materialID);

        List<Shape> shapes = shapeRepository.findByShape(shapeComboBox.getSelectionModel().getSelectedItem().toString());

        for (Shape shape : shapes) {
            shapeID = shape.getShapeId();
        }

        job.setShapeId(shapeID);

        // set customerId using the customer record created at the beginning of this method
        job.setCustomerId(customer2.getCustomerId());


        job.setJobtitle(materialComboBox.getValue().toString() + " " + colorComboBox.getValue().toString() + " " +
                shapeComboBox.getValue().toString());

        // set the dateOfSale for the job to today's date
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        job.setDateofsale(date);

        // save job
        job3Repository.save(job);

        stageManager.switchScene(FxmlView.KANBAN);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        materialComboBox.getItems().removeAll(materialComboBox.getItems());
        materialComboBox.getItems().addAll("Burlap", "Cotton", "Velvet", "Silk");
        materialComboBox.getSelectionModel().select("Burlap");

        colorComboBox.getItems().removeAll(colorComboBox.getItems());
        colorComboBox.getItems().addAll("White", "Red", "Green", "Gold");
        colorComboBox.getSelectionModel().select("White");

        shapeComboBox.getItems().removeAll(shapeComboBox.getItems());
        shapeComboBox.getItems().addAll("Sock", "Bone", "Candy Cane", "Paw Print");
        shapeComboBox.getSelectionModel().select("Sock");

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        DateOfSaleLabel.setText(date.toString());


    }

    /**
     * Method that uses the component price to calculate the total price of the product, including
     * State Tax of 3.4% and a Markup of 10%
     */
    public void calculateJobTotal(){
        double num1;
        double num2;
        double num3;
        double subtotal;
        double total;
        double markup;
        double stateTax;

        num1 = Double.parseDouble(materialPriceLabel.getText());
        num2 = Double.parseDouble(colorPriceLabel.getText());
        num3 = Double.parseDouble(shapePriceLabel.getText());
        subtotal = num1 + num2 + num3;
        markup = (.10*subtotal);
        subtotal = subtotal +markup;
        markupLabel.setText(Double.toString(markup));
        stateTax = .034*subtotal;
        stateTaxLabel.setText(Double.toString(stateTax));
        total = subtotal+stateTax;

        totalLabel.setText(Double.toString(total));
    }

    /**
     * Switch Scenes
     * @param actionEvent
     * @throws IOException
     */
    public void homeButtonPushed(ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.KANBAN);
    }

    @FXML
    private void backButtonPushed (ActionEvent actionEvent) throws IOException {
        stageManager.switchScene(FxmlView.NEWCUSTOMER);
    }
}
