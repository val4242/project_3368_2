package project_3368.project_3368.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.CustomerRepository;
import project_3368.project_3368.Models.Customer;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;
import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class ManageCustomersController implements Initializable {

    /**
     * This Controller allows the Shop owner to create, update, delete and view customers
     */

    static Customer customer;

    @Lazy
    @Autowired
    StageManager stageManager;

    @Autowired
    CustomerRepository customerRepository;

    @FXML
    private TableView<Customer> customerTable;

    @FXML
    private TableColumn<Customer, Integer> customerIDCol;

    @FXML
    private TableColumn<Customer, String> firstNameColumn;

    @FXML
    private TableColumn<Customer, String> lastNameColumn;

    @FXML
    private TableColumn<Customer, String> phoneColumn;

    @FXML
    private TableColumn<Customer, String> emailColumn;

    @FXML
    private Button HomeButton;

    @FXML
    private Button addCustomerButton;

    @FXML
    private Button updateCustomerButton;

    @FXML
    private Button deleteCustomerButton;

    /**
     * Saves the selected customer to the static variable customer and switches stages to the update customer stage.
     * @param actionEvent
     */

    public void updateCustomerButtonPushed(ActionEvent actionEvent) {
        customer = customerTable.getSelectionModel().getSelectedItem();
        stageManager.switchScene(FxmlView.UPDATE_CUSTOMER);
    }

    /**
     * When User selects a customer from the tableview and presses the delete button, this method is executed and
     * deletes the corresponding selected customer from the database. The Tableview is reloaded with the updated data.
     * @param actionEvent
     */

    public void deleteCustomerButtonPushed(ActionEvent actionEvent) {
        customer = customerTable.getSelectionModel().getSelectedItem();
        customerRepository.delete(customer);
        initializeTV();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTV();
    }

    /**
     * InitializeTV is called in the Initialize method, this populates the tableview
     */

    public void initializeTV(){
        ObservableList<Customer> obsCustomerList = FXCollections.observableArrayList();

        Iterable<Customer> customerList = customerRepository.findAll();

        customerList.forEach(customer->obsCustomerList.add(customer));

        customerIDCol.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));

        customerTable.setItems(obsCustomerList);
        customerTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Change Scenes
     * @param actionEvent
     */

    public void HomeButtonPushed(ActionEvent actionEvent) {stageManager.switchScene(FxmlView.KANBAN); }

    public void addCustomerButtonPushed(ActionEvent actionEvent) { stageManager.switchScene(FxmlView.ADD_CUSTOMER); }
}
