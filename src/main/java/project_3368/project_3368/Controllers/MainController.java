package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;

@Controller
public class MainController {

    @Lazy
    @Autowired
    StageManager stageManager;

    /**
     * Switch Stages
     */
    public void loginButtonPushed(ActionEvent actionEvent) { stageManager.switchScene(FxmlView.KANBAN); }

    public void back(ActionEvent actionEvent){ stageManager.switchScene(FxmlView.JOB);
    }
}
