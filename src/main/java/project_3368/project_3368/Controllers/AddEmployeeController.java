package project_3368.project_3368.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import project_3368.project_3368.Repositories.EmployeeRepository;
import project_3368.project_3368.Models.Employee;
import project_3368.project_3368.view.FxmlView;
import project_3368.project_3368.view.StageManager;



@Controller
public class AddEmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Lazy
    @Autowired
    StageManager stageManager;

    @FXML
    private TextField FNameTxtBox;

    @FXML
    private TextField PositionTxtBox;

    @FXML
    private TextField StatusTxtBox;

    @FXML
    private TextField PhoneTxtBox;

    @FXML
    private TextField EmailTxtBox;

    @FXML
    private TextField LNameTxtBox;

    @FXML
    private TextField AddressTxtBox;

    @FXML
    private TextField CityTxtBox;

    @FXML
    private TextField StateTxtBox;

    @FXML
    private TextField ZipTxtBox;

    @FXML
    private TextField EmpNumTxtBox;

    @FXML
    private Button BackButton;

    @FXML
    private Button HomeButton;

    @FXML
    private Button AddButton;

    /**
     * Once the User inputs all the Employee info and clicks the add button, the program creates a new employee, sets all
     * of the information based on the text inputted in the textboxes and then saves the employee to the database.
     * @param event
     */

    @FXML
    void AddButtonPushed(ActionEvent event) {

        int check = checkInput();

        if (check == 0) {
            Employee employee = new Employee();
            employee.setEmployeeNumber(EmpNumTxtBox.getText());
            employee.setFirstName(FNameTxtBox.getText());
            employee.setLastName(LNameTxtBox.getText());
            employee.setAddress(AddressTxtBox.getText());
            employee.setCity(CityTxtBox.getText());
            employee.setEmail(EmailTxtBox.getText());
            employee.setPhoneNumber(PhoneTxtBox.getText());
            employee.setPosition(PositionTxtBox.getText());
            employee.setState(StateTxtBox.getText());
            employee.setStatus(StatusTxtBox.getText());
            employee.setZipcode(ZipTxtBox.getText());

            employeeRepository.save(employee);

            stageManager.switchScene(FxmlView.MANAGEEMPLOYEES);
        }

    }

    /**
     * Validation -- check if all fields are filled in
     * @return
     */
    public int checkInput() {
        if (EmpNumTxtBox.getText().isEmpty() || FNameTxtBox.getText().isEmpty() || LNameTxtBox.getText().isEmpty()
                || AddressTxtBox.getText().isEmpty() || CityTxtBox.getText().isEmpty() ||EmailTxtBox.getText().isEmpty() ||
                PhoneTxtBox.getText().isEmpty() || PositionTxtBox.getText().isEmpty() || StateTxtBox.getText().isEmpty()
                 || StatusTxtBox.getText().isEmpty() || ZipTxtBox.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please input all information!");
            alert.showAndWait();
            return 1;
        }
        return 0;
    }

    /**
     * Switch Stages
     * @param event
     */

    @FXML
    void BackButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);

    }

    @FXML
    void HomeButtonPushed(ActionEvent event) {
        stageManager.switchScene(FxmlView.KANBAN);
    }

}
