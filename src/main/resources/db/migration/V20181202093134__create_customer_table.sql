CREATE TABLE public.Customer
(
    CUSTOMER_ID serial PRIMARY KEY NOT NULL,
    FIRST_NAME varchar(25),
    LAST_NAME varchar(25),
    PHONE_NUMBER varchar(10),
    EMAIL varchar(50),
    ADDRESS varchar(60),
    CITY VARCHAR(40),
    STATE varchar(2),
    ZIPCODE varchar(5)
);
CREATE UNIQUE INDEX Customer_CUSTOMER_ID_uindex ON public.Customer (CUSTOMER_ID);