CREATE TABLE public.job_2
(
    JOB_ID serial PRIMARY KEY NOT NULL,
    Total DOUBLE PRECISION,
    MATERIAL_ID int,
    COLOR_ID int,
    SHAPE_ID int,
    STATUS varchar(25),
    CUSTOMER_ID int
);
CREATE UNIQUE INDEX job_2_JOB_ID_uindex ON public.job_2 (JOB_ID);