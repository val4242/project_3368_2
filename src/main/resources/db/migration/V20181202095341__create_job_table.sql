CREATE TABLE public.Job
(
    JOB_ID serial PRIMARY KEY NOT NULL,
    TOTAL double precision,
    MATERIAL_ID int,
    COLOR_ID int,
    SHAPE_ID int,
    STATUS varchar(25),
    CUSTOMER_ID int,
    CONSTRAINT Job___fk2 FOREIGN KEY (COLOR_ID) REFERENCES public.color (color_id),
    CONSTRAINT Job___fk3 FOREIGN KEY (SHAPE_ID) REFERENCES public.shape (shape_id),
    CONSTRAINT Job___fk4 FOREIGN KEY (CUSTOMER_ID) REFERENCES public.customer (customer_id),
    CONSTRAINT Job___fk1 FOREIGN KEY (MATERIAL_ID) REFERENCES public.material (material_id)
);
CREATE UNIQUE INDEX Job_JOB_ID_uindex ON public.Job (JOB_ID);
CREATE UNIQUE INDEX Job_CUSTOMER_ID_uindex ON public.Job (CUSTOMER_ID);