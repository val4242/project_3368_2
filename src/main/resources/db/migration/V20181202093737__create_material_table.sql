CREATE TABLE public.Material
(
    MATERIAL_ID serial PRIMARY KEY NOT NULL,
    Material varchar(25),
    Price double precision
);
CREATE UNIQUE INDEX Material_MATERIAL_ID_uindex ON public.Material (MATERIAL_ID);