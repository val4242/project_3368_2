CREATE TABLE public.Employee
(
    EMPLOYEE_ID serial PRIMARY KEY NOT NULL,
    FIRST_NAME varchar(25),
    LAST_NAME varchar(25),
    POSITION varchar(25),
    STATUS varchar(25),
    PHONE_NUMBER varchar(25),
    EMAIL varchar(40),
    ADDRESS varchar(50),
    CITY varchar(30),
    STATE varchar(2),
    ZIPCODE varchar(5),
    EMPLOYEE_NUMBER varchar(10)
);
CREATE UNIQUE INDEX Employee_EMPLOYEE_ID_uindex ON public.Employee (EMPLOYEE_ID);