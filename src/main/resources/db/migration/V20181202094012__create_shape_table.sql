CREATE TABLE public.Shape
(
    SHAPE_ID serial PRIMARY KEY NOT NULL,
    SHAPE varchar(25),
    PRICE double precision
);
CREATE UNIQUE INDEX Shape_SHAPE_ID_uindex ON public.Shape (SHAPE_ID);