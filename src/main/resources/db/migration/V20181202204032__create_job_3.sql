CREATE TABLE public.job_3
(
    job_id serial PRIMARY KEY NOT NULL,
    total DOUBLE PRECISION,
    material_id int,
    color_id int,
    shape_id int,
    status varchar(25),
    customer_id int,
    DateOfSale date
);
CREATE UNIQUE INDEX job_3_job_id_uindex ON public.job_3 (job_id);