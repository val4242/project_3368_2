CREATE TABLE public.Color
(
    COLOR_ID serial PRIMARY KEY NOT NULL,
    COLOR varchar(25),
    PRICE double precision
);
CREATE UNIQUE INDEX Color_COLOR_ID_uindex ON public.Color (COLOR_ID);